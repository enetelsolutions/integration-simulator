﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NSportSimulator
{
    public partial class FormMain : Form
    {
        private DExpressForm dExpressForm;
        private MPSoftwareForm mpSoftwareForm;
        private OrderPaymentForm orderPaymentForm;

        public string ServerUrl { get { return this.txtServerUrl.Text; }}

        public FormMain()
        {
            Application.ThreadException += new ThreadExceptionEventHandler(ShowUnhandledException);

            InitializeComponent();

            this.txtServerUrl.Text = "https://www.nsport.enetelsolutions.com/";
            //this.txtServerUrl.Text = "http://10.0.34.185:8080/";

            dExpressForm = new DExpressForm(this);
            mpSoftwareForm = new MPSoftwareForm(this);
            orderPaymentForm = new OrderPaymentForm(this);

            this.tabPageDExp.Controls.Clear();
            this.tabPageDExp.Controls.Add(dExpressForm);

            this.tabPageMPSW.Controls.Clear();
            this.tabPageMPSW.Controls.Add(mpSoftwareForm);

            this.tabPageOrderPayment.Controls.Clear();
            this.tabPageOrderPayment.Controls.Add(orderPaymentForm);
        }

        private static void ShowUnhandledException(object sender, ThreadExceptionEventArgs t)
        {
            MessageBox.Show(t.Exception.Message, "Stevo sta si sjebo !!!");
        }

        private void tabOptions_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.tabOptions.SelectedTab == this.tabPageMPSW)
                mpSoftwareForm.CheckLoadData();
            else if (this.tabOptions.SelectedTab == this.tabPageOrderPayment)
                orderPaymentForm.CheckLoadData();
        }

        private void btnReload_Click(object sender, EventArgs e)
        {
            if (this.tabOptions.SelectedTab == this.tabPageMPSW)
                mpSoftwareForm.ReloadData();
            else if (this.tabOptions.SelectedTab == this.tabPageOrderPayment)
                orderPaymentForm.ReloadData();
        }
    }
}
