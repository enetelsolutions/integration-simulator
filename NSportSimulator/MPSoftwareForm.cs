﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NSportSimulator
{
    public partial class MPSoftwareForm : Form
    {
        private class GetModel
        {
            public class Product
            {
                public string product_sku = "";
                public string product_size = "";
                public string quantity = "";
            }
            public class Suborder
            {
                public string id = "";
                public List<Product> products = new List<Product>();
            }
            public class Shop
            {
                public string shop_id = "";
                public string code = "";
                public string type = "";
            }

            public List<Suborder> suborders = new List<Suborder>();
            public List<Shop> shops = new List<Shop>();
        }

        private class PostModel
        {
            public class SKUItem
            {
                public string sku;
                public string size;
                public uint quantity;
            }

            public string shop_id;
            public string order_id;
            public int total_packages;
            public List<SKUItem> products = new List<SKUItem>();
        }


        private GetModel modelData;
        private DateTime modelDataLastGetTime;
        private FormMain mainForm;

        public MPSoftwareForm(FormMain mainForm)
        {
            InitializeComponent();

            this.mainForm = mainForm;
            this.TopLevel = false;
            this.FormBorderStyle = FormBorderStyle.None;
            this.Visible = true;
            this.Dock = DockStyle.Fill;

            this.txtApiPath.Text = "/api/test/mp-sofware";
        }

        private void ToggleMessage(bool show, string message = null, bool isError = false)
        {
            this.lblMessage.Visible = show;
            if (show)
            {
                this.lblMessage.Text = message;
                this.lblMessage.ForeColor = isError ? Color.Red : Color.Green;
            }
        }

        public void CheckLoadData()
        {
            if ((DateTime.UtcNow - modelDataLastGetTime).TotalMinutes > 5)
                LoadData();
        }

        public void ReloadData()
        {
            LoadData();
        }

        async private void LoadData(bool skipLoadingMessage = false)
        {
            try
            {
                if (skipLoadingMessage == false)
                    ToggleMessage(true, "Loading data ...", false);

                HttpClient client = Utils.CreateClient(Utils.ClientType.NSportAuth);
                var response = await client.GetAsync(Utils.ComineUrlPaths(this.mainForm.ServerUrl, txtApiPath.Text));
                var responseString = (await response.Content.ReadAsStringAsync()).Trim();

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    modelData = JsonConvert.DeserializeObject<GetModel>(responseString);
                    modelDataLastGetTime = DateTime.UtcNow;

                    ddlSuborder.SelectedItem = null;
                    ddlSuborder.Items.Clear();
                    ddlShop.SelectedItem = null;
                    ddlShop.Items.Clear();

                    if (modelData.suborders != null)
                    {
                        foreach (var order in modelData.suborders)
                        {
                            if (order.id == null)
                                continue;

                            ddlSuborder.Items.Add(new ComboboxItem { Value = order.id, Text = order.id });
                        }
                            
                    }

                    if (modelData.shops != null)
                    {
                        foreach (var shop in modelData.shops)
                        {
                            if (shop.type == null || shop.code == null)
                                continue;

                            ddlShop.Items.Add(new ComboboxItem { Value = shop.code, Text = shop.type + " " + shop.code });
                        }
                            
                    }

                    if (skipLoadingMessage == false)
                        ToggleMessage(false);
                }
                else
                {
                    ToggleMessage(true, "Unable to get MP software data\n" + Utils.GetResponseMessage(response), true);
                }
            }
            catch (Exception ex)
            {
                ToggleMessage(true, "Unable to get MP software data\n" + ex.Message, true);
            }
        }

        private void ddlSuborder_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.dataGridProducts.Rows.Clear();
            var selectedSuborder = ddlSuborder.SelectedItem as ComboboxItem;

            if (selectedSuborder == null || selectedSuborder.Value == null)
                return;

            var subOrderData = modelData.suborders.Find(x => x.id == selectedSuborder.Value.ToString());

            if (subOrderData != null && subOrderData.products != null)
            {
                foreach (var product in subOrderData.products)
                    this.dataGridProducts.Rows.Add(product.product_sku, product.product_size, product.quantity);
            }
        }

        async private void btnSave_Click(object sender, EventArgs e)
        {
            var postData = new PostModel();

            var selSuborder = (ddlSuborder.SelectedItem as ComboboxItem);
            if (selSuborder == null || selSuborder.Value == null)
            {
                ToggleMessage(true, "Please select suborder", true);
                return;
            }

            var selShop = (ddlShop.SelectedItem as ComboboxItem);
            if (selShop == null || selShop.Value == null)
            {
                ToggleMessage(true, "Please select shop", true);
                return;
            }

            postData.order_id = selSuborder.Value.ToString();
            postData.shop_id = selShop.Value.ToString();
            postData.total_packages = (int)numTotalPackages.Value;

            foreach (DataGridViewRow row in this.dataGridNewProducts.Rows)
            {
                var skuName = row.Cells["NewSKU"].Value?.ToString().Trim();
                var sizeStr = row.Cells["NewSize"].Value?.ToString().Trim();
                var quantityStr = row.Cells["NewQuantity"].Value?.ToString().Trim();


                if (skuName == null || quantityStr == null || sizeStr == null || skuName.Length == 0 || sizeStr.Length == 0)
                    continue;

                uint quantity = 1;
                if (quantityStr.Length > 0 && UInt32.TryParse(quantityStr, out quantity) == false)
                    continue;

                postData.products.Add(new PostModel.SKUItem
                {
                    sku = skuName,
                    size = sizeStr,
                    quantity = quantity
                });
            }

            if (postData.products.Count == 0)
            {
                ToggleMessage(true, "Please provide valid data to send to server", true);
                return;
            }

            try
            {
                ToggleMessage(true, "Saving records ...", false);

                HttpClient client = Utils.CreateClient(Utils.ClientType.NSportAuth);
                //var content = new StringContent(JsonConvert.SerializeObject(postData), Encoding.UTF8, "application/json");
                //content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                var content = Utils.SerializeJsonData(postData);
                var response = await client.PostAsync(Utils.ComineUrlPaths(this.mainForm.ServerUrl, txtApiPath.Text), content);
                var responseString = (await response.Content.ReadAsStringAsync()).Trim();

                if (responseString != null && responseString.Length > 0)
                {
                    var message = JsonConvert.DeserializeObject<Utils.GenericMessage>(responseString);
                    ToggleMessage(true, message.message, response.StatusCode != System.Net.HttpStatusCode.OK);

                    LoadData(true);
                    this.dataGridNewProducts.Rows.Clear();
                }
                else
                {
                    ToggleMessage(true, Utils.GetResponseMessage(response), response.StatusCode != System.Net.HttpStatusCode.OK);
                }

            }
            catch (Exception ex)
            {
                ToggleMessage(true, "Unable to send request\n" + ex.Message, true);
            }
        }
    }
}
