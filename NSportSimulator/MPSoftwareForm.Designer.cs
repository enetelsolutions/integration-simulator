﻿namespace NSportSimulator
{
    partial class MPSoftwareForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label10 = new System.Windows.Forms.Label();
            this.numTotalPackages = new System.Windows.Forms.NumericUpDown();
            this.ddlShop = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.lblMessage = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.dataGridNewProducts = new System.Windows.Forms.DataGridView();
            this.NewSKU = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NewSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NewQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label8 = new System.Windows.Forms.Label();
            this.dataGridProducts = new System.Windows.Forms.DataGridView();
            this.SKU = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ddlSuborder = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtApiPath = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numTotalPackages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridNewProducts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridProducts)).BeginInit();
            this.SuspendLayout();
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(365, 446);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(81, 13);
            this.label10.TabIndex = 26;
            this.label10.Text = "Total packages";
            // 
            // numTotalPackages
            // 
            this.numTotalPackages.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.numTotalPackages.Location = new System.Drawing.Point(452, 444);
            this.numTotalPackages.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numTotalPackages.Name = "numTotalPackages";
            this.numTotalPackages.Size = new System.Drawing.Size(132, 20);
            this.numTotalPackages.TabIndex = 25;
            this.numTotalPackages.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ddlShop
            // 
            this.ddlShop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ddlShop.FormattingEnabled = true;
            this.ddlShop.Location = new System.Drawing.Point(294, 79);
            this.ddlShop.Name = "ddlShop";
            this.ddlShop.Size = new System.Drawing.Size(290, 21);
            this.ddlShop.TabIndex = 24;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(294, 63);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(32, 13);
            this.label9.TabIndex = 23;
            this.label9.Text = "Shop";
            // 
            // lblMessage
            // 
            this.lblMessage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMessage.Location = new System.Drawing.Point(9, 470);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(575, 70);
            this.lblMessage.TabIndex = 22;
            this.lblMessage.Text = "label9";
            this.lblMessage.Visible = false;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSave.Location = new System.Drawing.Point(15, 444);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 21;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // dataGridNewProducts
            // 
            this.dataGridNewProducts.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridNewProducts.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridNewProducts.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridNewProducts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridNewProducts.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NewSKU,
            this.NewSize,
            this.NewQuantity});
            this.dataGridNewProducts.Location = new System.Drawing.Point(12, 288);
            this.dataGridNewProducts.Name = "dataGridNewProducts";
            this.dataGridNewProducts.Size = new System.Drawing.Size(572, 150);
            this.dataGridNewProducts.TabIndex = 20;
            // 
            // NewSKU
            // 
            this.NewSKU.HeaderText = "SKU";
            this.NewSKU.Name = "NewSKU";
            // 
            // NewSize
            // 
            this.NewSize.HeaderText = "Size";
            this.NewSize.Name = "NewSize";
            // 
            // NewQuantity
            // 
            this.NewQuantity.HeaderText = "Quantity";
            this.NewQuantity.Name = "NewQuantity";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 272);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(74, 13);
            this.label8.TabIndex = 19;
            this.label8.Text = "New Products";
            // 
            // dataGridProducts
            // 
            this.dataGridProducts.AllowUserToAddRows = false;
            this.dataGridProducts.AllowUserToDeleteRows = false;
            this.dataGridProducts.AllowUserToResizeRows = false;
            this.dataGridProducts.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridProducts.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridProducts.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridProducts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridProducts.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SKU,
            this.ProductSize,
            this.Quantity});
            this.dataGridProducts.Location = new System.Drawing.Point(12, 119);
            this.dataGridProducts.Name = "dataGridProducts";
            this.dataGridProducts.ReadOnly = true;
            this.dataGridProducts.Size = new System.Drawing.Size(572, 150);
            this.dataGridProducts.TabIndex = 18;
            // 
            // SKU
            // 
            this.SKU.HeaderText = "SKU";
            this.SKU.Name = "SKU";
            this.SKU.ReadOnly = true;
            // 
            // ProductSize
            // 
            this.ProductSize.HeaderText = "Size";
            this.ProductSize.Name = "ProductSize";
            this.ProductSize.ReadOnly = true;
            // 
            // Quantity
            // 
            this.Quantity.HeaderText = "Quantity";
            this.Quantity.Name = "Quantity";
            this.Quantity.ReadOnly = true;
            // 
            // ddlSuborder
            // 
            this.ddlSuborder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ddlSuborder.FormattingEnabled = true;
            this.ddlSuborder.Location = new System.Drawing.Point(12, 79);
            this.ddlSuborder.Name = "ddlSuborder";
            this.ddlSuborder.Size = new System.Drawing.Size(276, 21);
            this.ddlSuborder.TabIndex = 17;
            this.ddlSuborder.SelectedIndexChanged += new System.EventHandler(this.ddlSuborder_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 63);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "Suborder";
            // 
            // txtApiPath
            // 
            this.txtApiPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtApiPath.Location = new System.Drawing.Point(12, 25);
            this.txtApiPath.Name = "txtApiPath";
            this.txtApiPath.Size = new System.Drawing.Size(572, 20);
            this.txtApiPath.TabIndex = 15;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "API path";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 103);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 13);
            this.label7.TabIndex = 27;
            this.label7.Text = "Products";
            // 
            // MPSoftwareForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(596, 549);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.numTotalPackages);
            this.Controls.Add(this.ddlShop);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.lblMessage);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.dataGridNewProducts);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.dataGridProducts);
            this.Controls.Add(this.ddlSuborder);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtApiPath);
            this.Controls.Add(this.label5);
            this.Name = "MPSoftwareForm";
            this.Text = "MPSoftwareForm";
            ((System.ComponentModel.ISupportInitialize)(this.numTotalPackages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridNewProducts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridProducts)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown numTotalPackages;
        private System.Windows.Forms.ComboBox ddlShop;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.DataGridView dataGridNewProducts;
        private System.Windows.Forms.DataGridViewTextBoxColumn NewSKU;
        private System.Windows.Forms.DataGridViewTextBoxColumn NewSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn NewQuantity;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataGridView dataGridProducts;
        private System.Windows.Forms.DataGridViewTextBoxColumn SKU;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn Quantity;
        private System.Windows.Forms.ComboBox ddlSuborder;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtApiPath;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
    }
}