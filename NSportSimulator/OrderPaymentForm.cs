﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NSportSimulator
{
    public partial class OrderPaymentForm : Form
    {
        private class GetModel
        {
            public class Order
            {
                public string order_code = "";
                public string order_id = "";
            }

            public List<Order> orders = new List<Order>();
        }

        private GetModel modelData;
        private DateTime modelDataLastGetTime;
        private FormMain mainForm;

        public OrderPaymentForm(FormMain mainForm)
        {
            InitializeComponent();

            this.mainForm = mainForm;
            this.TopLevel = false;
            this.FormBorderStyle = FormBorderStyle.None;
            this.Visible = true;
            this.Dock = DockStyle.Fill;

            this.txtApiPath.Text = "/api/test/payment";
        }

        private void ToggleMessage(bool show, string message = null, bool isError = false)
        {
            this.lblMessage.Visible = show;
            if (show)
            {
                this.lblMessage.Text = message;
                this.lblMessage.ForeColor = isError ? Color.Red : Color.Green;
            }
        }

        public void CheckLoadData()
        {
            if ((DateTime.UtcNow - modelDataLastGetTime).TotalMinutes > 5)
                LoadData();
        }

        public void ReloadData()
        {
            LoadData();
        }

        async private void LoadData(bool skipLoadingMessage = false)
        {
            try
            {
                if (skipLoadingMessage == false)
                    ToggleMessage(true, "Loading data ...", false);

                HttpClient client = Utils.CreateClient(Utils.ClientType.NSportAuth);
                var response = await client.GetAsync(Utils.ComineUrlPaths(this.mainForm.ServerUrl, txtApiPath.Text));
                var responseString = (await response.Content.ReadAsStringAsync()).Trim();

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    modelData = JsonConvert.DeserializeObject<GetModel>(responseString);
                    modelDataLastGetTime = DateTime.UtcNow;

                    ddlOrder.SelectedItem = null;
                    ddlOrder.Items.Clear();
                    
                    if (modelData.orders != null)
                    {
                        foreach (var order in modelData.orders)
                        {
                            if (order.order_code == null)
                                continue;

                            ddlOrder.Items.Add(new ComboboxItem { Value = order.order_id, Text = order.order_code });
                        }
                    }

                    if (skipLoadingMessage == false)
                        ToggleMessage(false);
                }
                else
                {
                    ToggleMessage(true, "Unable to get orders data\n" + Utils.GetResponseMessage(response), true);
                }
            }
            catch (Exception ex)
            {
                ToggleMessage(true, "Unable to get orders data\n" + ex.Message, true);
            }
        }

        async private void btnUpdate_Click(object sender, EventArgs e)
        {
            var selOrder = (ddlOrder.SelectedItem as ComboboxItem);
            if (selOrder == null || selOrder.Value == null)
            {
                ToggleMessage(true, "Please select order", true);
                return;
            }

            if (rbPaid.Checked == false && rbNotPaid.Checked == false)
            {
                ToggleMessage(true, "Please select if order is payed or not", true);
                return;
            }

            var values = new Dictionary<string, string>
            {
               { "order_id", selOrder.Value.ToString() },
               { "paid", rbPaid.Checked ? "1" : "0" }
            };

            try
            {
                ToggleMessage(true, "Updating record ...", false);

                HttpClient client = Utils.CreateClient(Utils.ClientType.NSportAuth);
                var content = Utils.SerializeFormData(values);
                var response = await client.PostAsync(Utils.ComineUrlPaths(this.mainForm.ServerUrl, txtApiPath.Text), content);
                var responseString = (await response.Content.ReadAsStringAsync()).Trim();

                if (responseString.Length > 0)
                {
                    var message = JsonConvert.DeserializeObject<Utils.GenericMessage>(responseString);
                    ToggleMessage(true, message.message, response.StatusCode != System.Net.HttpStatusCode.OK);
                }
                else
                {
                    ToggleMessage(true, Utils.GetResponseMessage(response), response.StatusCode != System.Net.HttpStatusCode.OK);
                }
            }
            catch (Exception ex)
            {
                ToggleMessage(true, "Unable to send request\n" + ex.Message, true);
            }
        }
    }
}
