﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NSportSimulator
{
    public partial class DExpressForm : Form
    {
        FormMain mainForm;

        public DExpressForm(FormMain mainForm)
        {
            InitializeComponent();

            this.mainForm = mainForm;
            this.TopLevel = false;
            this.FormBorderStyle = FormBorderStyle.None;
            this.Visible = true;
            this.Dock = DockStyle.Fill;

            this.txtAPIPath.Text = "/api/test/suborder";

            this.ddlStatus.Items.Add(new ComboboxItem { Value = 0, Text = "Priprema za dostavu" });
            this.ddlStatus.Items.Add(new ComboboxItem { Value = 1, Text = "Pošiljka je isporučena" });
            this.ddlStatus.Items.Add(new ComboboxItem { Value = 5, Text = "Pošiljka je odbijena od strane primaoca" });
            this.ddlStatus.Items.Add(new ComboboxItem { Value = 6, Text = "Pokušana isporuka, nema nikoga na adresi" });
            this.ddlStatus.Items.Add(new ComboboxItem { Value = 7, Text = "Pokušana isporuka, primalac je na godišnjem odmoru" });
            this.ddlStatus.Items.Add(new ComboboxItem { Value = 8, Text = "Pokušana isporuka, netačna je adresa primaoca" });
            this.ddlStatus.Items.Add(new ComboboxItem { Value = 9, Text = "Pokušana isporuka, primalac nema novac" });
            this.ddlStatus.Items.Add(new ComboboxItem { Value = 10, Text = "Sadržaj pošiljke nije odgovarajući" });
            this.ddlStatus.Items.Add(new ComboboxItem { Value = 11, Text = "Pošiljka je oštećena - reklamacioni postupak" });
            this.ddlStatus.Items.Add(new ComboboxItem { Value = 12, Text = "Isporuka odložena u dogovoru sa primaocem" });
            this.ddlStatus.Items.Add(new ComboboxItem { Value = 20, Text = "Pošiljka je vraćena pošiljaocu" });
            this.ddlStatus.Items.Add(new ComboboxItem { Value = 21, Text = "Pošiljka se vraća pošiljaocu" });
            this.ddlStatus.Items.Add(new ComboboxItem { Value = 105, Text = "Pošiljka je odbijena od strane primaoca (105)" });
            this.ddlStatus.Items.Add(new ComboboxItem { Value = 106, Text = "Ponovni pokušaj isporuke, nema nikoga na adresi (106)" });
            this.ddlStatus.Items.Add(new ComboboxItem { Value = 107, Text = "Ponovni pokušaj isporuke, primalac je na odmoru (107)" });
            this.ddlStatus.Items.Add(new ComboboxItem { Value = 108, Text = "Ponovni pokušaj isporuke, netačna adresa primaoca (108)" });
            this.ddlStatus.Items.Add(new ComboboxItem { Value = 109, Text = "Ponovni pokušaj isporuke, primalac nema novac (109)" });
            this.ddlStatus.Items.Add(new ComboboxItem { Value = 110, Text = "Sadržaj pošiljke nije odgovarajući (110)" });
            this.ddlStatus.Items.Add(new ComboboxItem { Value = 111, Text = "Pošiljka je oštećena - reklamacioni postupak (111)" });
            this.ddlStatus.Items.Add(new ComboboxItem { Value = 112, Text = "Isporuka odložena u dogovoru sa primaocem (112)" });
        }

        private void ToggleMessage(bool show, string message = null, bool isError = false)
        {
            this.lblMessage.Visible = show;
            if (show)
            {
                this.lblMessage.Text = message;
                this.lblMessage.ForeColor = isError ? Color.Red : Color.Green;
            }
        }

        async private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (this.txtSuborderCode.Text.Trim().Length == 0 || this.ddlStatus.SelectedItem == null || (this.ddlStatus.SelectedItem as ComboboxItem) == null)
            {
                ToggleMessage(true, "Please fill all fields", true);
                return;
            }

            var values = new Dictionary<string, string>
            {
               { "suborder_code", this.txtSuborderCode.Text },
               { "suborder_status", ((int)(this.ddlStatus.SelectedItem as ComboboxItem).Value).ToString() }
            };

            try
            {
                ToggleMessage(true, "Updating record ...", false);

                HttpClient client = Utils.CreateClient(Utils.ClientType.NoAuth);
                var content = Utils.SerializeFormData(values);
                var response = await client.PostAsync(Utils.ComineUrlPaths(mainForm.ServerUrl, txtAPIPath.Text), content);
                var responseString = (await response.Content.ReadAsStringAsync()).Trim();

                if (responseString.Length > 0)
                {
                    var message = JsonConvert.DeserializeObject<Utils.GenericMessage>(responseString);
                    ToggleMessage(true, message.message, response.StatusCode != System.Net.HttpStatusCode.OK);
                }
                else
                {
                    ToggleMessage(true, Utils.GetResponseMessage(response), response.StatusCode != System.Net.HttpStatusCode.OK);
                }
            }
            catch (Exception ex)
            {
                ToggleMessage(true, "Unable to send request\n" + ex.Message, true);
            }
        }
    }
}
