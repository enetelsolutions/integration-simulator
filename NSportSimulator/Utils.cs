﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace NSportSimulator
{
    public static class Utils
    {
        public enum ClientType
        {
            NoAuth = 1,
            NSportAuth = 2,
        }
        public class GenericMessage
        {
            public string message = "";
        }

        public static string ComineUrlPaths(string root, string path)
        {
            if (root.EndsWith("/") == true)
                root = root.Remove(root.Length - 1);
            if (path.StartsWith("/") == true)
                path = path.Remove(0, 1);

            return root + "/" + path;
        }

        public static string GetResponseMessage(HttpResponseMessage response)
        {
            string message = "";
            if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
                message += "Unable to connect to server, ";
            if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                message += "Unable to connect to server, ";

            message += ((int)response.StatusCode).ToString() + ": " + response.ReasonPhrase;
            return message;
        }

        public static HttpClient CreateClient(ClientType type)
        {
            HttpClient client = new HttpClient();

            if (type == ClientType.NSportAuth)
            {
                // it is stupid to keep username and password here, but this is for internal use :)
                var byteArray = Encoding.ASCII.GetBytes("MPS_NSPORT:N_5p0Rt-wE!3");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", "xxxxxxxxxxxxxxxxxxxx");
            }

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            return client;
        }

        public static StringContent SerializeJsonData(object data)
        {
            var content = new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json");
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            return content;
        }

        public static FormUrlEncodedContent SerializeFormData(Dictionary<string, string> data)
        {
            var content = new FormUrlEncodedContent(data);
            return content;
        }
    }
}
