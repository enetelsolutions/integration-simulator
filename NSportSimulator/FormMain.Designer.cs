﻿namespace NSportSimulator
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtServerUrl = new System.Windows.Forms.TextBox();
            this.tabOptions = new System.Windows.Forms.TabControl();
            this.tabPageDExp = new System.Windows.Forms.TabPage();
            this.tabPageMPSW = new System.Windows.Forms.TabPage();
            this.tabPageOrderPayment = new System.Windows.Forms.TabPage();
            this.btnReload = new System.Windows.Forms.Button();
            this.tabOptions.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Server";
            // 
            // txtServerUrl
            // 
            this.txtServerUrl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtServerUrl.Location = new System.Drawing.Point(56, 12);
            this.txtServerUrl.Name = "txtServerUrl";
            this.txtServerUrl.Size = new System.Drawing.Size(471, 20);
            this.txtServerUrl.TabIndex = 1;
            // 
            // tabOptions
            // 
            this.tabOptions.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabOptions.Controls.Add(this.tabPageDExp);
            this.tabOptions.Controls.Add(this.tabPageMPSW);
            this.tabOptions.Controls.Add(this.tabPageOrderPayment);
            this.tabOptions.Location = new System.Drawing.Point(12, 39);
            this.tabOptions.Name = "tabOptions";
            this.tabOptions.SelectedIndex = 0;
            this.tabOptions.Size = new System.Drawing.Size(600, 546);
            this.tabOptions.TabIndex = 2;
            this.tabOptions.SelectedIndexChanged += new System.EventHandler(this.tabOptions_SelectedIndexChanged);
            // 
            // tabPageDExp
            // 
            this.tabPageDExp.Location = new System.Drawing.Point(4, 22);
            this.tabPageDExp.Margin = new System.Windows.Forms.Padding(0);
            this.tabPageDExp.Name = "tabPageDExp";
            this.tabPageDExp.Size = new System.Drawing.Size(592, 520);
            this.tabPageDExp.TabIndex = 0;
            this.tabPageDExp.Text = "DExpress";
            this.tabPageDExp.UseVisualStyleBackColor = true;
            // 
            // tabPageMPSW
            // 
            this.tabPageMPSW.Location = new System.Drawing.Point(4, 22);
            this.tabPageMPSW.Margin = new System.Windows.Forms.Padding(0);
            this.tabPageMPSW.Name = "tabPageMPSW";
            this.tabPageMPSW.Size = new System.Drawing.Size(592, 520);
            this.tabPageMPSW.TabIndex = 1;
            this.tabPageMPSW.Text = "MPSW";
            this.tabPageMPSW.UseVisualStyleBackColor = true;
            // 
            // tabPageOrderPayment
            // 
            this.tabPageOrderPayment.Location = new System.Drawing.Point(4, 22);
            this.tabPageOrderPayment.Margin = new System.Windows.Forms.Padding(0);
            this.tabPageOrderPayment.Name = "tabPageOrderPayment";
            this.tabPageOrderPayment.Size = new System.Drawing.Size(592, 520);
            this.tabPageOrderPayment.TabIndex = 2;
            this.tabPageOrderPayment.Text = "Order payment";
            this.tabPageOrderPayment.UseVisualStyleBackColor = true;
            // 
            // btnReload
            // 
            this.btnReload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReload.Location = new System.Drawing.Point(533, 12);
            this.btnReload.Name = "btnReload";
            this.btnReload.Size = new System.Drawing.Size(75, 20);
            this.btnReload.TabIndex = 3;
            this.btnReload.Text = "Reload";
            this.btnReload.UseVisualStyleBackColor = true;
            this.btnReload.Click += new System.EventHandler(this.btnReload_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(624, 597);
            this.Controls.Add(this.btnReload);
            this.Controls.Add(this.tabOptions);
            this.Controls.Add(this.txtServerUrl);
            this.Controls.Add(this.label1);
            this.MinimumSize = new System.Drawing.Size(600, 630);
            this.Name = "FormMain";
            this.Text = "NSport Simulator";
            this.tabOptions.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtServerUrl;
        private System.Windows.Forms.TabControl tabOptions;
        private System.Windows.Forms.TabPage tabPageDExp;
        private System.Windows.Forms.TabPage tabPageMPSW;
        private System.Windows.Forms.TabPage tabPageOrderPayment;
        private System.Windows.Forms.Button btnReload;
    }
}

